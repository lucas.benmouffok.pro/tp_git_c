#include <stdio.h>
#include <stdlib.h>


int main()
{
    printf("hello \n");
    //Variables
    int nb=0;
    int note=0;
    float moyenne=0;

    //Initialisation des variables
    nb=0;
    note=0;

    printf("Entrez le nombre d'eleves!\n");
    scanf("%d", &nb);
    printf("Nombre d'eleves: %d\n", nb);

    int max=-1;
    int min=21;

    for(int i=0;i<nb;i++){
    printf("Entrez la note de l'eleve %d\n", i+1);
    scanf("%d", &note);
        while((note<0)||(note >20)){
            printf("La note saisie n'est pas valide, veuillez la re-saisir.\n");
            scanf("%d", &note);
        }
        printf("Note saisie: %d\n\n", note);
        if(note>=10){
            printf("L'eleve %d est admis \n", i+1);
        }
        else{
            printf("L'eleve %d n'est pas admis \n", i+1);
        }
        if(note<min){
            min = note;
        }
        if(note > max){
            max = note;
        }
        moyenne += note;
    }

    printf("La note max est : %d\n", max);
    printf("La note min est : %d\n", min);
    moyenne = moyenne / nb;
    printf("La moyenne de la classe est: %f\n", moyenne);


    return 0;
}
